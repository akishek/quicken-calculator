<?php

namespace Quicken;

/**
 * Router routes requests to controller methods. 
 * 
 * @author Alfred Kishek <akishek22@gmail.com>
 */
class Router 
{
    /**
     * $routes holds an array of all the routes. The routes are in a standard format
     * of CLASS:HANDLER@METHOD
     * 
     * @var array of Slim routes $routes
     */
    protected $routes;

    /**
     * Holds the users http request.  
     * 
     * @var Request part of the Slim Http namespace
     */
    protected $request;
    
    /**
     * Used for handling any 404 errors with routing.
     * 
     * @var function
     */
    protected $errorHandler;

    public function __construct()
    {
        $env = \Slim\Environment::getInstance();
        $this->request = new \Slim\Http\Request($env);
        $this->routes = array();
    }

    /**
     * Adds an array of routes to the Router. Routes are defined in a 
     * CLASS:HANDLER@METHOD where Class is the controller, Handler is the 
     * function to handle request, and a HTTP method (Any by default)
     * 
     * @param array $routes
     *
     */
    public function addRoutes($routes)
    {
        foreach ($routes as $route => $path) {
            $this->addRoute($route, $path);
        }
    }

    /**
     * Adds a single Slim route to the $routes.
     * 
     * @param string $route
     * @param string @path
     * 
     */
    public function addRoute($route, $path) {
        $method = 'any';
     
        if (strpos($path, '@') !== false) {
            list($path, $method) = explode('@', $path);
        }

        $func = $this->processCallback($path);

        $r = new \Slim\Route($route, $func);
        $r->setHttpMethods(strtoupper($method));

        $this->routes[] = $r;
    }

    /**
     * Processes the callback function defined by the users path. 
     * Callback functions exist in the controller and handle the route. 
     * Default class is Main. Default handler is index.
     * 
     * @param string $path
     * 
     * @return function closure to the route.
     */
    protected function processCallback($path)
    {
        // Default class is main if path has no defined class. 
        $class = 'Main';

        if (strpos($path, ':') !== false) {
            list($class, $path) = explode(':', $path);
        }

        $function = ($path != '') ? $path : 'index';

        /**
         * Closure to be returned as the callback for the route. 
         * 
         * @param string $class name of the class that holds the callback function.
         * @param string $function name of the function.
         * 
         * @return mixed response from the user function.
         */
        $func = function () use ($class, $function) {
            $class = '\Controller\\' . $class;
            $class = new $class();

            $args = func_get_args();

            return call_user_func_array(array($class, $function), $args);
        };

        return $func;
    }
    
    public function set404Handler($path)
    {
        $this->errorHandler = $this->processCallback($path);
    }

    /**
     * Processes the routes and calls any routes that match the $routes array.
     * Calls 404 Error handler. If no error handler is set, echos 404 error.
     * 
     */
    public function run()
    {
        $display404 = true;
        $uri = $this->request->getResourceUri();
        $method = $this->request->getMethod();

        foreach ($this->routes as $index => $route) {
            if ($route->matches($uri)) {
                if ($route->supportsHttpMethod($method) || $route->supportsHttpMethod('ANY')) {
                    call_user_func_array($route->getCallable(), array_values($route->getParams()));
                    $display404 = false;
                }
            }
        }

        if ($display404) {
            if (is_callable($this->errorHandler)) {
                call_user_func($this->errorHandler);
            } else {
                // If no 404 error handler is set.
                echo '404 - Route not found';
            }
        }
    }
}
