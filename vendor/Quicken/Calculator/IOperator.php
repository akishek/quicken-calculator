<?php

namespace Quicken\Calculator;

/**
 * Interface for a generic operator. Operators can be developed and added to
 * the calculator class. Calculator class can evaluate operations. This allows
 * for multiple developers to develop operations in parallel.
 *
 */
interface IOperator
{
    public function evaluate(array $operands = array());
}
