<?php

namespace Quicken\Calculator\Operator;

/**
 * Multiplication operation.
 * 
 * @author Alfred Kishek <akishek22@gmail.com>
 */
class Multiplication implements \Quicken\Calculator\IOperator {
    public function evaluate(array $operands = array()) {
        $result = array_shift($operands);
        foreach($operands as $value) {
            $result = $result * $value;
        }
        return $result;
    }
}
