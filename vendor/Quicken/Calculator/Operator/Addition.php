<?php

namespace Quicken\Calculator\Operator;

/**
 * Addition operation.
 * 
 * @author Alfred Kishek <akishek22@gmail.com>
 */
class Addition implements \Quicken\Calculator\IOperator {
    public function evaluate(array $operands = array()) {
        return array_sum($operands);
    }
}
