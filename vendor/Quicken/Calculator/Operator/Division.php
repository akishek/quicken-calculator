<?php

namespace Quicken\Calculator\Operator;
use \Exception as Exception;

/**
 * Division operation.
 * 
 * @author Alfred Kishek <akishek22@gmail.com>
 */
class Division implements \Quicken\Calculator\IOperator {
    /**
     * Evaluates an array of operands by dividing each one by the next. 
     * 
     * @param array $operands
     * 
     * @throws Exception if attempt to divide by zero. 
     * 
     * @return string value of the division
     */
    public function evaluate(array $operands = array()) {
        $result = array_shift($operands);
        foreach($operands as $value) {
            if($value && is_numeric($value)) {
                $result = $result / $value;
            } else {
                throw new Exception('Division by zero.');
            }
        }
        return $result;
    }
}
