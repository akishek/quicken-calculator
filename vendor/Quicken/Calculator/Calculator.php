<?php

namespace Quicken\Calculator;

/**
 * Calculator class that calculates any custom operation that implements IOperator.
 * Operations are independent of the Calculator. This allows for many operations 
 * to be developed at a time. 
 * 
 * @author Alfred Kishek <akishek22@gmail.com>
 */
class Calculator
{
    /**
     * Calculator uses $operator to evaluate the custom operation.
     *
     * @var IOperator exists within Quicken\Calculator namespace
     */
    protected $operator;

    /**
     * Operator used to store an array of operands in the calculation.
     * The operation is performed on all the operands from left to right.
     * 
     * @var array
     */
    protected $operands; 
    
    /**
     * Set the operator.
     * 
     * @param IOperator $operator
     * 
     * @return Calculator
     */
    public function setOperator(\Quicken\Calculator\IOperator $operator) 
    {
        $this->operator = $operator;
        return $this;
    }
    
    /**
     * Set the operands.
     * 
     * @param array $operands default array()
     * 
     * @return Calculator
     */
    public function setOperands($operands = array()) 
    {
        $this->operands = $operands;
        return $this;
    }
    
    /**
     * Adds one operand.
     * 
     * @param float $operand
     * 
     * @return Calculator
     */
    public function addOperand($operand) 
    {
        $this->operands[] = $operand;
        return $this;
    }
    
    /**
     * Evaluates the operator given the operands
     * 
     * @return float as the calculated value. 
     */
    public function calculate() 
    {
        return $this->operator->evaluate($this->operands);
    }
}
