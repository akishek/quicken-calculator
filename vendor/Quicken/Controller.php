<?php

namespace Quicken;

/**
 * Base controller class, extends Slim controllers. Sets the model defined in the
 * settings into the context of the controller. Also responsible for rendering templates. 
 * Templates can have missing .php ext, one will be added. 
 * 
 * @author Alfred Kishek <akishek22@gmail.com>
 */
Class Controller extends \Slim\Slim
{
    /**
     * Holds the given model by the settings.
     *
     * @var object $data
     */
    protected $data;

    public function __construct()
    {
        $settings = require('../settings.php');
        if (isset($settings['model'])) {
            $this->data = $settings['model'];
        }
        parent::__construct($settings);
    }

    public function render($name, $data = array(), $status = null)
    {
        if (strpos($name, '.php') === false) {
            $name = $name . '.php';
        }
        parent::render($name, $data, $status);
    }
}
