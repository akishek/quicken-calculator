<?php

$settings = array(
    'view' => new \Slim\Views\Twig(),
    'templates.path' => '../Views',
    'model' => (Object)array(
        'calculator'   => new \Quicken\Calculator\Calculator,
        // Used to define the available operators
        'operator_map' => array(
            '+' => 'Addition', 
            '-' => 'Subtraction',
            '/' => 'Division',
            '*' => 'Multiplication'
        )
    )
);

return $settings;
