<?php 

namespace Controller;

/**
 * Controller that handle the main routes.
 *
 * @author Alfred Kishek <akishek22@gmail.com>
 */
Class Main extends \Quicken\Controller
{
    /**
     * Landing page view. Renders calculator template with available operators.
     * 
     */
    public function index()
    {
        $operators = $this->data->operator_map;
        $this->render('landing', array('title'=>'Quicken Calculator', 'operators' => $operators));
    }
    
    /**
     * Endpoint for calculator to validate and perform calculations on operands. 
     * Method: POST 
     * Status: 200, 400
     * 
     * @return string json with success/error object with values/errors
     */
    public function calculate()
    {
        $errors = array();

        $operands = $_POST['numbers'];
        $operation = $_POST['operation'];
        
        // Small serverside validation.
        $is_valid_operation = in_array($operation, array_values($this->data->operator_map));
        $are_valid_operands = $this->_areValidOperands($operands);
        
        if($is_valid_operation && $are_valid_operands) {
            try {
                $value = $this->_calculate($operands, $operation);
                
                $response = array(
                    'error' => false,
                    'value' => $value
                );

                http_response_code(200);
                echo json_encode($response);
            } catch(\Exception $e) {
                $error_msg = $e->getMessage();
                $errors[] = $error_msg;
            }
        } else {
            $error_msg = 'Invalid operator or operands.';
            $errors[] = $error_msg;
        }

        if(!empty($errors)) {
            $error_obj = array(
                'errors' => $errors,
                'error'  => true
            );
            
            http_response_code(400);
            echo json_encode($error_obj);
        }
    }
    
    public function notFound() {
        echo 'Uh oh! This is not the route you were looking for.';
    }
    
    private function _areValidOperands(array $operands) {
        foreach($operands as $operand) {
            if(!is_numeric($operand)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Evaluates the operation against the operands. The operation instansiates
     * an operation class and executes evaluate on the operation.
     * 
     * @param array  $operands
     * @param string $operation
     * 
     * @return float
     */
    private function _calculate($operands, $operation) {
        $calc = $this->data->calculator;
        $operator_class = '\Quicken\Calculator\Operator\\' . $operation;
        $operator = new $operator_class();
        $value = $calc->setOperands($operands)
                      ->setOperator($operator)
                      ->calculate();
        return $value;
    }
}
