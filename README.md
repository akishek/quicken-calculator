#Quick'en Easy Calculator!

Introduction
---
All of the main logic exists in the vendor/Quicken directory. 

The logic for the calculator exists in the "Quicken\Calculator" namespace. The controller and the router are there for a basic MVC framework built off of the micro-framework [Slim](http://www.slimframework.com/).
 
 
        +-- Quicken
            |   +-- Calculator
                |   +-- Operator
                    |   +-- Addition.php
                    |   +-- Division.php
                    |   +-- Multiplication.php
                    |   +-- Subtraction.php
            |   +-- Calculator.php
            |   +-- IOperator.php
        |   +-- Controller.php
        |   +-- Router.php

Design
---
The main Calculator class uses polymorphism to be independent from the operators. This removes the need for basic conditional statements.

The advantage:  code is also much easier to read and allows for developers to add multiple operations without having to write code in the same Calculator class file.  

Usage
---
In the implementation of the Calculator you can use it as such: 

    $calculator = new \Quicken\Calculator\Calculator;

    $calculator->setOperands(1,2);
    $calculator->setOperator(new \Quicken\Calculator\Operator\Addition);
    $calculator->calculate();

You can also chain: 

      $calc->setOperands($operands)
           ->setOperator($operator)
           ->calculate();
