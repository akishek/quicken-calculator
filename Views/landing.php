{% extends "_base.php" %}
{% block title %}{{title}}{% endblock %}
{% block styles %}
    {{parent()}}
    <link rel="stylesheet" href="css/calculator.css" type="text/css" />
{% endblock %}

{% block header %}
    <h1 id="quicken-header">Quick'en Easy <span id="sub-header">Calculator</span></h1>
    <p>Developed by Alfred Kishek</p>
{% endblock %}

{% block content %}
    <div id="answer-block"></div>
    <form id="calculator">
        <input type="decimal" class="operand" type="text" name="numbers[]"/>
        <select class="operators" name="operation">
            {% for operator, operation in operators %}
                <option value="{{operation}}">{{operator}}</option>
            {% endfor %}
        </select>
        <input type="decimal" class="operand" type="text" name="numbers[]"/>
        <button type="submit" id="calculate_btn" class="btn btn-lg btn-primary">Calculate</button>
    </form>
    <div id="errors"></div>
{% endblock %}

{% block scripts %}
    {{ parent() }}
    <script type="text/javascript" src="js/landing.js"></script>
{% endblock %}