'use strict';

$(document).ready(function() {
    
    // Calculator form submit. 
    $('#calculator').submit(function(e) {
        e.preventDefault();
        // Endpoint for the calculator
        var calculate_url = '/calculate';

        $.ajax({
            type: 'POST',
            url: calculate_url,
            data: $('#calculator').serialize(),
            success: function(response) {
                var data = JSON.parse(response);
                $('#answer-block').text(data.value);
                $('#errors').html('');
            }, 
            error: function(response) {
                var data = JSON.parse(response.responseText);
                var errors_div = ''; 
                
                $.each(data.errors, function(index, err) {
                    errors_div += '<p>Error: ' + err + '</p>';
                });
                
                $('#answer-block').html('');
                $('#errors').html(errors_div);
            }
        });
    });
});
