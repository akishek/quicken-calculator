<?php

require '../vendor/autoload.php';

$router = new \Quicken\Router;
$routes = array(
	'/' => 'Main:index@get',
	'/calculate' => 'Main:calculate@post'
);

$router->addRoutes($routes);
$router->set404Handler('Main:notFound');
$router->run();
